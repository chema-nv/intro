// import {Header} from './components'
// import { Footer } from './components';
import { Header, Footer} from './components';
import './App.scss';

//DOS FORMAS DE CREAR COMPONENTES
// const Saludo = () => {return <h1>hola, componete funciona con sintaxis ES6</h1>}
// function SaludosEs5(){return <h1>hola, componete funciona con sintaxis ES5</h1>}

function App() {
  return (
    <div className="app">
      <Header />
      <div>ESTE ES EL CONTENIDO DE MI APICACION</div>
      <Footer />

      {/* DOS FORMAS DE LLAMAR COMPONENTES
      <Saludo></Saludo>
      <SaludosEs5 /> */}
    </div>
  );
}

export default App;
