import Header from './Header/Header';
import Footer from './Footer/Footer';
import Text from './Text/Text';

export {
    Header,
    Footer,
    Text,
}