import './Header.scss'
// const Header = () => {
//     return <h2>Esto es el Header</h2>;
// };
//eSTAS LINEA SE PUEDE CAMBIAN POR LA SIGUENTE AL RETORNAR 
// UN UNICO ELEMENTO
//const Header = () => <h2>Esto es el Header</h2>;

const Header = () => {
    return (
    <header className='header'>
        <h2 className='header__title'>Esto es el Header</h2>
        <p className='header__text'>Esto es un parrafo dentre del Header</p>
    </header>
    );
     };

export default Header;